import json
from django.test import TestCase
from foodies_rest.models import TruckUser, Truck


class TrucksApiTest(TestCase):
    def setUp(self):
        self.owner = TruckUser.objects.create(
            first="Owner",
            last="Truck",
            avatar="avatar",
            email="email@email.com",
            username="owner_truck",
        )
        self.truck = Truck.objects.create(
            name="Plink",
            website="http://plinko.example.com",
            category="American",
            vegetarian_friendly=True,
            owner=self.owner,
        )

    def test_get_trucks(self):
        response = self.client.get("/api/trucks/")
        content = response.json()
        self.assertEqual(response.status_code, 200)
        for truck in content["trucks"]:
            if truck["name"] == self.truck.name:
                self.assertEqual(truck["id"], self.truck.id)
                self.assertEqual(truck["owner"]["id"], self.truck.owner.id)

    def test_create_truck(self):
        data = json.dumps(
            {
                "name": "Plink",
                "website": "http://plinko.example.com",
                "category": "American",
                "vegetarian_friendly": True,
                "owner_id": 2,
            }
        )
        response = self.client.post(
            "/api/trucks/",
            data,
            content_type="application/json",
        )
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(data["id"])
