from common.json import ModelEncoder
from .models import Truck, TruckUser


class TruckUserEncoder(ModelEncoder):
    model = TruckUser
    properties = [
        "id",
        "first",
        "last",
        "avatar",
        "email",
        "username",
    ]


class TruckEncoder(ModelEncoder):
    model = Truck
    properties = [
        "id",
        "name",
        "website",
        "category",
        "vegetarian_friendly",
        "owner",
    ]
    encoders = {
        "owner": TruckUserEncoder(),
    }
