from django.contrib import admin
from .models import TruckUser, Truck


admin.site.register(TruckUser)
admin.site.register(Truck)
