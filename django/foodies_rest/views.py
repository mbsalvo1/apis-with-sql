from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TruckEncoder
from .models import Truck, TruckUser


@require_http_methods(["GET", "POST"])
def truck_list(request):
    if request.method == "GET":
        trucks = Truck.objects.all()
        result = {
            "trucks": trucks,
        }
        return JsonResponse(
            result,
            encoder=TruckEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            response = JsonResponse({"message": "Bad JSON"})
            response.status_code = 400
            return response

        try:
            owner_id = content["owner_id"]
            content["owner"] = TruckUser.objects.get(id=owner_id)
        except TruckUser.DoesNotExist:
            response = JsonResponse({"message": "Owner does not exist"})
            response.status_code = 400
            return response

        truck = Truck.objects.create(**content)
        return JsonResponse(
            truck,
            encoder=TruckEncoder,
            safe=False,
        )
