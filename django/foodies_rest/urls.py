from django.urls import path
from .views import truck_list

urlpatterns = [
    path("api/trucks/", truck_list, name="truck_list"),
]
