import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import OwnerForm from "./OwnerForm";
import OwnerList from './OwnerList';
import TruckList from './TruckList';

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <BrowserRouter basename={basename}>
      <div className="container">
        <Nav />
        <Routes>
          <Route path="/trucks" element={<TruckList />} />
          <Route path="/owners" element={<OwnerList />} />
          <Route path="/owners/new" element={<OwnerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
