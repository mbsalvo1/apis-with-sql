import { Link } from 'react-router-dom';
import { useGetOwnersQuery } from './store/ownersApi';
import ErrorNotification from './ErrorNotification';

function OwnerList() {
  const { data, error, isLoading } = useGetOwnersQuery();

  if (isLoading) {
    return (
      <progress className="progress is-primary" max="100"></progress>
    );
  }

  return (
    <div className="columns is-centered">
      <div className="column is-narrow">
        <ErrorNotification error={error} />
        <div className="field has-text-right">
          <Link to="/owners/new" className="button">Add owner</Link>
        </div>
        <table className="table is-striped">
          <thead>
            <tr>
              <th>First</th>
              <th>Last</th>
              <th>Email</th>
              <th>User name</th>
            </tr>
          </thead>
          <tbody>
            {data.users.map(owner => (
              <tr key={owner.id}>
                <td>{owner.first}</td>
                <td>{owner.last}</td>
                <td>{owner.email}</td>
                <td>{owner.username}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default OwnerList;
