import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const ownersApi = createApi({
  reducerPath: 'owners',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_FAST_API,
  }),
  tagTypes: ['OwnerList'],
  endpoints: builder => ({
    getOwners: builder.query({
      query: () => '/api/users',
      providesTags: ['OwnerList'],
    }),
    createOwner: builder.mutation({
      query: data => ({
        url: '/api/users',
        body: data,
        method: 'post',
      }),
      invalidatesTags: ['OwnerList'],
    }),
  }),
});

export const {
  useGetOwnersQuery,
  useCreateOwnerMutation,
} = ownersApi;
