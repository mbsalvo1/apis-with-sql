import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import { ownersApi } from './ownersApi';
import { trucksApi } from './trucksApi';

export const store = configureStore({
  reducer: {
    [trucksApi.reducerPath]: trucksApi.reducer,
    [ownersApi.reducerPath]: ownersApi.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      .concat(trucksApi.middleware)
      .concat(ownersApi.middleware),
});

setupListeners(store.dispatch);
