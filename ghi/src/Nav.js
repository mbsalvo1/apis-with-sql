import { NavLink, useLocation } from "react-router-dom";
import { useGetTrucksQuery } from './store/trucksApi';
import { useGetOwnersQuery } from './store/ownersApi';

function Nav() {
  const location = useLocation();
  const { data: truckData } = useGetTrucksQuery();
  const { data: ownerData } = useGetOwnersQuery();

  return (
    <div className="tabs is-centered has-background-link-light is-boxed">
      <ul>
        <li className={location.pathname.startsWith('/owners') ? 'is-active' : '' }>
          <NavLink to="/owners">
            Owners {ownerData ? `(${ownerData.users.length})` : ''}
          </NavLink>
        </li>
        <li className={!location.pathname.startsWith('/owners') ? 'is-active' : '' }>
          <NavLink to="/trucks">
            Trucks {truckData ? `(${truckData.trucks.length})` : ''}
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Nav;
