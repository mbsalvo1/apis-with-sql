from fastapi.testclient import TestClient
from main import app
from db import MenuItemQueries


client = TestClient(app)


class FakeMenuItemQueries:
    called = False

    def get_menu_items_for_truck(self, truck_id):
        FakeMenuItemQueries.called = True
        return [
            {
                "id": 1,
                "name": "Diet Cookies",
                "calories": 25,
            },
            {
                "id": 10,
                "name": "Diet Milkshake",
                "calories": 10,
            },
        ]


def test_get_all_trucks():
    # Arrange
    FakeMenuItemQueries.called = False
    app.dependency_overrides[MenuItemQueries] = FakeMenuItemQueries
    expected = {
        "menu_items": [
            {
                "id": 1,
                "name": "Diet Cookies",
                "calories": 25,
            },
            {
                "id": 10,
                "name": "Diet Milkshake",
                "calories": 10,
            },
        ]
    }

    # Act
    response = client.get("/api/trucks/1/menu_items")

    # Clean up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert FakeMenuItemQueries.called
    assert response.json() == expected
